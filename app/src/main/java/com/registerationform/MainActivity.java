package com.registerationform;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    EditText firstName, lastName, address, city, state, country, pinCode, mobileNumber, Email;
    Button submit;
    Context mContext = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_form);

        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        address = (EditText) findViewById(R.id.address);
        city = (EditText) findViewById(R.id.city);
        state = (EditText) findViewById(R.id.state);
        country = (EditText) findViewById(R.id.country);
        pinCode = (EditText) findViewById(R.id.pinCode);
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        Email = (EditText) findViewById(R.id.Email);
        submit = (Button) findViewById(R.id.submit);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate(firstName) && validate(lastName) && validate(address) && validate(city) && validate(state) && validate(country)
                        && validate(pinCode) && validate(mobileNumber))
                {

                    Toast.makeText(mContext,"SuccessFully Register",Toast.LENGTH_SHORT).show();
              //      Toast.makeText(MainActivity.this, "SuccessFully Register", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


        private boolean validate(EditText editText) {
        // check whether the field is empty or not
            if (editText.getText().toString().trim().length() < 1) {
        // display the error if field is empty
                editText.setError("Please Fill This!!!");
        // set focus on field so that cursor will automatically move to that field
                editText.requestFocus();
                return false;
            }
            return true;
        }


    }
